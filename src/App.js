import React from 'react';
import BackgroundImage from './components/BackgroundImage/BackgroundImage';
import WebsiteContainer from './components/WebsiteContainer/WebsiteContainer'

const bgImage = 'http://www.betheepicwin.com/wp-content/uploads/2017/07/32278942595_906bf67afd_o.jpg'

const App = () => {
  return (
    <div>
      <BackgroundImage bgImage={bgImage} />
      <WebsiteContainer />
    </div>
  )
}

export default App;