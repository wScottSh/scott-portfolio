import React from 'react';
import './NavBar.css';

const NavBar = (props) => {
  const navItems = props.items.map((item) => {
    return (
      <div className="navItem">
        <p>{item.title}</p>
        <img alt={`${item.title} Navigation Button`} src={item.img} />
      </div>
    )
  });

  return (
    <div className="navBar">
      {navItems}
    </div>
  )
};

export default NavBar;