import React from 'react';
import SiteHeader from '../SiteHeader/SiteHeader';
import './WebsiteContainer.css';
import siteLogo from '../../images/logo-horizontal-white.svg';

const WebsiteContainer = (props) => {
  let contentScreen = 'blog';

  const navBarItems = [
    {
      title: 'Forums',
      img: 'https://zeldauniverse.net/wp-content/themes/temple/images/forums.svg',
    },
    {
      title: 'Guides',
      img: 'https://zeldauniverse.net/wp-content/themes/temple/images/guides.svg',
    },
    {
      title: 'Features',
      img: 'https://zeldauniverse.net/wp-content/themes/temple/images/features.svg',
    },
    {
      title: 'Media',
      img: 'https://zeldauniverse.net/wp-content/themes/temple/images/media.svg',
    },
    {
      title: 'Zelda Wiki',
      img: 'https://zeldauniverse.net/wp-content/themes/temple/images/wiki.svg',
    },
    {
      title: 'Patreon',
      img: 'https://zeldauniverse.net/wp-content/themes/temple/images/store.svg',
    },
  ]

  const blogScreen = (
    <div className="mainGrid">
      <SiteHeader
        logo={siteLogo}
        navBarItems={navBarItems}
      />
      <div className="content"></div>
      <div className="footer"></div>
    </div>
  )
  
  if (contentScreen === 'blog') {
    return blogScreen;
  }

  return <div>The Landing Page</div>
};

export default WebsiteContainer;
