import React from 'react';
import './SiteHeader.css';
import NavBar from '../NavBar/NavBar';

const SiteHeader = (props) => {
  return (
    <div className="header">
      <NavBar 
        items={props.navBarItems}
      />
      <img className="logo" alt="Site Logo" src={props.logo} />
    </div>
  )
};

export default SiteHeader;