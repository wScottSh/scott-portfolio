import React from 'react';
import './BackgroundImage.css';

const BackgroundImage = (props) => {
  return (
    <div className="bgImage" style={{backgroundImage: `url(${props.bgImage})`}} />
  )
};

export default BackgroundImage;